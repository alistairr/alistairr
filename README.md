# About me
I'm a software engineering manager, a developer, and a finance researcher.

# 🖥️  Skills and Technology

* Programming: Go, Python, Postgres, HTMX, Turso
* Data: Mage, BigQuery, Looker
* Devops: GCP, Linux, Ansible, Docker, Jira, Confluence

# 🛳️  Projects

* Spartan - A lightweight issue tracking application
* Lean Project Valuation - Researching project valuation in R&D with entrepreneur and investor interactions
